
import 'dart:convert';

import 'dart:io';
import 'package:image/image.dart';
import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart';

class ImageService {

  static Future<File> resizeImage(File imageFile) async {
    Image image = decodeImage(imageFile.readAsBytesSync());
    Image resized;
    if (image.height > image.width) {
      resized = copyResize(image, height: 720);
    } else if (image.width > image.height) {
      resized = copyResize(image, width: 720);
    }
    String directory = (await getTemporaryDirectory()).path;
    File resizedImageFile = File('$directory/image_rsz.jpg')..writeAsBytesSync(encodeJpg(resized));

    return resizedImageFile;
  }

  static Future<String> encodeBase64(File image) async {

    return base64Encode((await image.readAsBytes()));
  }


  static Future<dynamic> getImageResult(String base64Image) async {
    String url = 'https://sigpy.herokuapp.com/predict';
    Map<String, String> body = {
      "plant_image": 'data:image/jpeg;base64,$base64Image'
    };
    Map<String, String> headers = {
      "Content-type": "application/json"
    };

    http.Response response = await http.post(url, headers: headers, body: jsonEncode(body));
    print(response.body);
    return jsonDecode(response.body);
  }

}
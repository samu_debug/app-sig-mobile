import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'main.controller.dart';
class MainPageView extends StatefulWidget {
  @override
  _MainPageViewState createState() => _MainPageViewState();
}

class _MainPageViewState extends StateMVC<MainPageView> {
  MainPageController _con;
  @override
  void initState() {
    super.initState();
    String id = add(MainPageController());
    _con = controllerById(id);

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFEEEEEE),
      body: _body(),
    );
  }

  Container _body() {
    return Container(
      child: Column(
        children: <Widget>[
          Flexible(
            flex: 2,
            child: Container(
              child: Center(
                child: Image(
                  image: AssetImage('assets/images/logo.png'),
                ),
              ),
            ),
          ),
          Flexible(
            child: Container(
              child: Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,

                  children: <Widget>[
                    InkWell(
                      onTap: () {
                        _con.selectImage("camera");
                      },
                      child: Material(
                        elevation: 4,
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            color: Colors.white,
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.only(top: 16),
                                child: Icon(Icons.camera_enhance, size: 48, color: Color(0xBF60C000),),
                              ),
                              Padding(
                                padding: EdgeInsets.only(left: 16, right: 16, bottom: 16),
                                child: Text("Tirar com a câmera", style: TextStyle(fontStyle: FontStyle.italic, fontSize: 18, fontFamily: 'RobotoCondensed'),),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        _con.selectImage("gallery");
                      },
                      child: Material(
                        elevation: 4,
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            color: Colors.white,
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.only(top: 16),
                                child: Icon(Icons.photo_library, size: 48, color: Color(0xBF60C000),),
                              ),
                              Padding(
                                padding: EdgeInsets.only(left: 16, right: 16, bottom: 16),
                                child: Text("Escolher da Galeria", style: TextStyle(fontStyle: FontStyle.italic, fontSize: 18, fontFamily: 'RobotoCondensed'),),
                              )
                            ],
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),


            ),
          ),
          Flexible(
            child: Container(),
          )
        ],
      ),
    );
  }
}

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:image_picker/image_picker.dart';
import '../result/result.view.dart';

class MainPageController extends ControllerMVC {


  void selectImage(String action) async {
    await _infoDialog();
    File image;
    switch (action) {
      case 'camera':
        image = await ImagePicker.pickImage(source: ImageSource.camera);
        break;
      case 'gallery':
        image = await ImagePicker.pickImage(source: ImageSource.gallery);
        break;
      default:
        print('Invalid Option!');
        break;
    }
    if (image != null) {
      Navigator.push(stateMVC.context, MaterialPageRoute(
          builder: (context) => ResultPageView(image: image,)
      ));
    }
  }

  Future<void> retrieveLostData() async {
    final LostDataResponse response = await ImagePicker.retrieveLostData();
    if (response == null) {
      return;
    }
    if (response.file != null) {
      File image = response.file;
      Navigator.push(stateMVC.context, MaterialPageRoute(
          builder: (context) => ResultPageView(image: image,)
      ));
    }
  }

  Future<void> _infoDialog() async {
    return showDialog(
        context: stateMVC.context,
        builder: (context) {
          return AlertDialog(
            title: Text("Informações"),
            content: Text(
                String.fromCharCode(0x2022) + " Escolha um local que esteja bem iluminado.\n" +
                String.fromCharCode(0x2022) + " Certifique-se de que a folha preencha maior area possível da imagem."
            ),
            actions: <Widget>[
              FlatButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text("Ok"),
              )
            ],
          );
        }
    );
  }

}
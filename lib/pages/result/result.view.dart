import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'result.controller.dart';

class ResultPageView extends StatefulWidget {

  final File image;

  ResultPageView({@required this.image});

  @override
  _ResultPageViewState createState() => _ResultPageViewState(this.image);
}

class _ResultPageViewState extends StateMVC<ResultPageView> {
  final File image;
  ResultPageController _con;

  _ResultPageViewState(this.image);

  @override
  void initState() {
    super.initState();
    String id = add(ResultPageController());
    _con = controllerById(id);
    _con.getImageResult(image);
  }

  @override
  void dispose() {
    super.dispose();
    _con.closeStream();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFEEEEEE),
      body: Column(

        children: <Widget>[
          Flexible(
            flex: 1,
            child: Container(),
          ),
          Flexible(
            flex: 2,
            child: Padding(
              padding: EdgeInsets.only(left: 16, right: 16),
              child: Container(

                decoration: BoxDecoration(

                  border: Border.all(
                    color: Color(0xBF60C000)
                  )
                ),
                child: Image.file(image, fit: BoxFit.scaleDown,),
              ),
            ),
          ),
          Flexible(
            flex: 3,
            child: StreamBuilder<String>(
              stream: _con.getResultStream(),
              initialData: 'waiting',
              builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
                if (snapshot.hasError) {
                  return Text("Erro!");
                } else {
                  switch(snapshot.data) {
                    case 'waiting':
                      return _loading();
                    case 'Sadia':
                      return _healthy();
                    case 'Doente':
                      return _infected();
                    default:
                      return Container();
                  }
                }
                return null;
              },
            ),
          )
        ],
      ),
    );
  }

  Widget _loading() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,

      children: <Widget>[
        Container(

          child: CircularProgressIndicator(
            value: null,
            strokeWidth: 5,
          ),
          height: 60,
          width: 60,
        ),
        Text("Avaliando...", style: TextStyle(fontSize: 25), textAlign: TextAlign.center,)
      ],
    );
  }

  Widget _healthy() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Container(

          child: Center(
            child: Icon(Icons.check_circle_outline, color: Color(0xFF7EFC00), size: 100, textDirection: TextDirection.ltr,),
          ),
          height: 60,
          width: 60,
        ),
        Text("Folha Aparentemente Saudável", style: TextStyle(fontSize: 25), textAlign: TextAlign.center,),
        Padding(
          padding: const EdgeInsets.only(left: 8.0, right: 8),
          child: FloatingActionButton.extended(
              onPressed: () {
                _con.goHome();
              },
              label: Text("Avaliar outra folha"),
              icon: Icon(Icons.settings_backup_restore),),
        )
      ],
    );
  }

  Widget _infected() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Container(

          child: Center(
            child: Icon(Icons.warning, color: Color(0xFFFFb800), size: 100, textDirection: TextDirection.ltr,),
          ),
          height: 60,
          width: 60,
        ),
        Text("Folha Possivelmente Doente", style: TextStyle(fontSize: 25), textAlign: TextAlign.center,),
        Padding(
          padding: const EdgeInsets.only(left: 8.0, right: 8),
          child: FloatingActionButton.extended(
            onPressed: () {
              _con.goHome();
            },
            label: Text("Avaliar outra folha"),
            icon: Icon(Icons.settings_backup_restore),),
        )
      ],
    );
  }


}

import 'dart:async';
import 'dart:io';


import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:flutter/material.dart';
import '../../services/image.service.dart';
import '../main/main.view.dart';


class ResultPageController extends ControllerMVC {
  StreamController<String> _resultStreamController = new StreamController();


  Stream getResultStream() {
    return _resultStreamController.stream;
  }

  void closeStream() {
    _resultStreamController.close();
  }

  void getImageResult(File image) async {
    File resizedImage = await ImageService.resizeImage(image);
    String base64Image = await ImageService.encodeBase64(resizedImage);
    dynamic imageResult = await ImageService.getImageResult(base64Image);
    _resultStreamController.add(imageResult['data']);
    closeStream();

  }

  void goHome() {
    Navigator.of(stateMVC.context).pushReplacement(MaterialPageRoute(
      builder: (context) => MainPageView()
    ));
  }
}